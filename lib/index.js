'use strict';

exports.register = (server, options, next) => {

	server.route({
		path: '/resource/upload-file',
		method: 'POST',
		config: {
			payload: {
				output: 'stream',
				parse: true,
				allow: 'multipart/form-data'
			}
		},
		handler: (request, reply) => {
			return require('./resources/upload-file')(request, reply);
		}
	});

	server.route({
		path: '/resource/{path*}',
		method: ['GET', 'POST'],
		handler: (request, reply) => {
			return require('./resources/' + request.params.path)(request, reply);
		}
	});

	next();

};

exports.register.attributes = {
	pkg: require('./package.json')
};
