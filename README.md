Skeleton Server v1
==================

Basic Skeleton Server that's plugged into `webpack` and uses my Sass library for styles/themes. Uses `Hapi` and `rethinkdb` for server setup & database usage.

This isn't running the micro app webpack system yet, but it'll get there. I hope?

### How do I get set up? ###

* Clone this repo
* Open Console/Terminal in repo directory
* type npm i
* run rethinkdb.exe
* type npm run dev

Features
--------

 - Supports Webpack build system
 - Builder lints
 - Supports Sass and Auto compiles scss files
 - Uses nodemon and watches file systems to auto restart and compile the server
 - Uses my Sass Library

## Non-Windows Users ##
You will need to modify the package.json and remove the `set` keyword from the dev script, this is not needed in anything outside Windows.

> "dev": "set DEBUG=sky* & npm-run-all --parallel dev:build dev:start"

Will become

> "dev": "DEBUG=sky* & npm-run-all --parallel dev:build dev:start"