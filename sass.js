'use strict';

const sass = require('node-sass');
const path = require('path');
const fs = require('fs');
const glob = require('glob');
const debug = require('debug')('sky:compile-sass');

let assetPath = 'public/css';

//Page specific sass
function compile() {
	glob(path.resolve('src/app/**/**/*.scss'), {}, (err, files) => {
		files.forEach((file) => {
			let entrySplit = file.substr(file.indexOf('app/'), file.indexOf('/index')).split('/');
			let fileLoc = entrySplit[1] + '/' + entrySplit[2];
			sass.render({
				file: file
			}, (err, result) => {
				if (!fs.existsSync(path.resolve(assetPath, entrySplit[1]))) fs.mkdirSync(path.resolve(assetPath, entrySplit[1]));
				if (!fs.existsSync(path.resolve(assetPath, fileLoc))) fs.mkdirSync(path.resolve(assetPath, fileLoc));

				fs.writeFileSync(path.resolve(assetPath, fileLoc, 'index.css'), result.css);
				debug('Compiled ' + fileLoc);
			});
		});
	});

	//Main website styles sass
	glob(path.resolve('public/scss/framework/framework.scss'), {}, (err, file) => {
		sass.render({
			file: file[0]
		}, (err, result) => {
			if (err) throw err;
			fs.writeFile(path.resolve(assetPath, 'default.css'), result.css, (err) => {
				if (err) throw err;
				debug('Compiled Main Theme -- Done');
			});
		});
	});
}

module.exports = compile();