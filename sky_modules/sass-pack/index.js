#! /usr/bin/env node
'use strict';

const fs = require('fs');
const sass = require('node-sass');
const path = require('path');
const debug = require('debug')('sky:sass-pack');
const program = require('commander');

program
	.version('1.0.0')
	.option('-s, --sourcepath [tpath]', 'Add a source path')
	.option('-t, --themepath [tpath]', 'Add a theme path')
	.option('-m, --manifestpath [mpath]', 'Add a manifest path')
	.option('-o, --output [opath]', 'Add the output path')
	.parse(process.argv);

	sassPack(program);
function sassPack(opts) {
	let manifest = JSON.parse(fs.readFileSync(opts.manifestpath, 'utf8'));
	getThemeSass();
	function getThemeSass() {
		let themeSass = fs.readdirSync(opts.themepath);
		let i = 0;
		let len = themeSass.length;
		for (i; i < len; i++) {
			let tmp = fs.readdirSync(path.join(opts.themepath, themeSass[i]));
			let name = themeSass[i].substring(themeSass[i].indexOf('themes')).replace('/', '');
			for (let x = 0; x < tmp.length; x++) {
				if (tmp[x].indexOf('-styles') > -1) {
					compile(name, fs.readFileSync(path.join(opts.themepath, themeSass[i], tmp[x]), 'utf8'));
				}
			}
		}
		getPageSass();
	}

	function getPageSass(loc) {
		let srcPath = loc || opts.sourcepath;
		let srcFiles = fs.readdirSync(srcPath);
		let i = 0;
		let len = srcFiles.length;
		for (i; i < len; i++) {
			if (srcFiles[i].indexOf('_components') > -1 || srcFiles[i].indexOf('_client') > -1) continue;
			if (fs.statSync(path.join(srcPath, srcFiles[i])).isDirectory()) getPageSass(path.join(srcPath, srcFiles[i]));

			if (srcFiles[i].indexOf('.scss') > -1) {
				let name = srcPath.substr(srcPath.indexOf('app')).replace(/app\\/g, '');
				if (!fs.readFileSync(path.join(srcPath, srcFiles[i]), 'utf8')) continue;
				compile(name.replace(/\\/g, '-'), fs.readFileSync(path.join(srcPath, srcFiles[i]), 'utf8'));
			}
		}
	}

	function compile(name, css) {
		sass.render({
			data: css
		}, (err, result) => {
			if (err) return debug(err);
			fs.writeFile(path.resolve(opts.output, name + '.css'), result.css, (err) => {
				if (err) throw err;
				debug('Compiled Main Theme -- Done');
				writeManifest(path.resolve(opts.output, name + '.css'), name);
			});
		});
	}

	function writeManifest(path, name) {
		if (!manifest.hasOwnProperty(name)) {
			manifest[name] = '/assets/css/' + name;
			fs.writeFileSync(opts.manifestpath, JSON.stringify(manifest), 'utf8');
		}
	}
}

module.exports = sassPack;