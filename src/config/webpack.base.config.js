'use strict';

const path = require('path');
const webpack = require('webpack');
const fs = require('fs');
const lintFormatter = require('eslint-friendly-formatter');
const nodeModulesPath 	= path.join(process.cwd(), 'node_modules');

module.exports = () => {
	let config = {

		//Get all JS file modules for Entry (Vendor is a common parent so all files will have one entry of Manifest)
		_buildEntries: function(p, chunksArr){
			chunksArr = chunksArr || [];
			fs.readdirSync(p).forEach(function(section){
				let newPath = path.join(p, section);
				if (fs.lstatSync(newPath).isDirectory()){
					config._buildEntries(newPath, chunksArr);
				} else if (section === 'client.js') {
					let scriptName = p.split('src\\app\\')[1]
						.replace(/\\/g, '-');
					config.entry[scriptName] = newPath;
					chunksArr.push(scriptName);
				}
			});
			return chunksArr;
		},

		//Add common vendor scripts to Alias list and noParse array
		_addVendor: function(name, uri){
			config.resolve.alias[name] = uri;
			config.module.noParse.push(new RegExp('^' + name + '$'));
		},

		name: 'lmjhphoto',

		//Default
		cache: true,

		//Default. Remove in Production!
		debug: true,
		devtool: 'cheap-module-eval-source-map',
		// Use this for IE8 Debugging ONLY
		// devtool: 'cheap-module-inline-source-map',

		//Report the first error as a hard error instead of tolerating it
		bail: true,

		//Default Common Vendor Scripts. Extend entry in App specific Config
		entry: {
			vendor:[
				'jquery',
				'handlebars',
				'bootstrap',
				'lodash'
			]
		},

		output: {
			path: path.join(process.cwd(), 'public/js/'),
			publicPath: '/assets/js/',
			//Default. Change in Production!
			filename: '[name].js',
			//Default. Remove in Production!
			pathinfo: true
		},

		resolve: {
			//Default
			root: process.cwd(),
			// Base Alias to be shared. Extend in App specific Config
			alias: {
				components: 'src/app/_components',
				'hbs_helpers': 'src/public/templates/helpers'
			}
		},

		//Base Default Plugins. Add more in App specific Config. CommonChunks are dangerous and need placed in the base because order matters.
		plugins: [
			new webpack.ProvidePlugin({
				$: 'jquery',
				jQuery: 'jquery',
				'window.jQuery': 'jquery',
				'_': 'lodash',
				'Handlebars': 'handlebars',
				bootstrap: 'bootstrap'
			}),
			new webpack.DefinePlugin({
				ENV: JSON.stringify(process.NODE_ENV)
			}),
			new webpack.optimize.DedupePlugin(),
			new webpack.optimize.UglifyJsPlugin({
				compress: {
					warnings: false
				}
			})
		],

		//Linting Default Formatter and Rules for all.
		eslint: {
			configFile: './.eslintrc',
			formatter: lintFormatter
		},

		// This is required due to a bug in jQuery's use of AMD Read more here:
		// http://alexomara.com/blog/webpack-and-jquery-include-only-the-parts-you-need/
		module: {

			//Default. Populated by addVendor method
			noParse: [],

			//Default Loaders for jQuery and json. Extendable in App specific Config
			loaders: [
				{
					test: /jquery\/src\/selector\.js$/,
					loader: 'amd-define-factory-patcher-loader'
				},
				{
					test: /\.json$/,
					loader: 'json'
				}
			],

			//Default PreLoader for linting. Required Components ignored for now most fail linting
			preLoaders: [
				{
					test: /\.js$/,
					loader: 'eslint-loader',
					exclude: [/components/]
				}
			],

			//I don't even know what this is... So beware.
			unknownContextRegExp: /$^/,
			unknownContextRecursive: false,
			unknownContextCritical: false,
			exprContextRegExp: /$^/,
			exprContextRecursive: false,
			exprContextCritical: false,
			wrappedContextRegExp: /$^/,
			wrappedContextRecursive: false,
			wrappedContextCritical: false
		},

		//Default Path that /node_modules/ resovles to
		resolveLoader: {
			root: nodeModulesPath
		}
	};

	config._buildEntries(path.join(process.cwd(), 'src/app/_components'));

	//Add Default shared Vendor scripts to Alias and noParse.
	//Be sure to use only original files, and not minified ones.  Minified ones break IE7 & 8.
	config._addVendor('jquery', 'node_modules/jquery/dist/jquery.js');
	config._addVendor('handlebars', 'node_modules/handlebars/dist/handlebars.js');
	config._addVendor('bootstrap', 'node_modules/bootstrap/dist/js/bootstrap.js');
	config._addVendor('lodash', 'node_modules/lodash/lodash.js');

	return config;
};