'use strict';

const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const apps = process.env.npm_package_config_build.split(',');
const AssetsPlugin = require('assets-webpack-plugin');

const assetsPluginInstance	= new AssetsPlugin({path: path.join(process.cwd(), 'src/config'), filename: 'js_manifest.json'});
let config = require(path.join(process.cwd(), 'src/config/webpack.base.config'))();

let getConfigs = function(p){
	fs.readdirSync(p).forEach(function(section){
		let newPath = path.join(p, section);
		if (fs.lstatSync(newPath).isDirectory()){
			if (apps.indexOf(section) === -1 || apps.indexOf('all') !== -1){
				getConfigs(newPath);
			}
		} else if (section === 'webpack.development.config.js') {

			config = require(newPath)(config);
		}
	});

};

getConfigs(path.join(process.cwd(), 'src/app/'));

// These Plugins must be last
config.plugins.push(
	new webpack.optimize.CommonsChunkPlugin({
		name: 'vendor',
		minChunks: Infinity
	})
);
config.plugins.push(
	new webpack.optimize.CommonsChunkPlugin({
		name: 'manifest'
	})
);
config.plugins.push(assetsPluginInstance);

// Production Overrides
if (process.env.NODE_ENV === 'production'){
	delete config.devtool;
	delete config.debug;
	delete config.output.pathinfo;
	config.output.filename = '[name].[chunkhash].js';
	config.output.chunkFilename = '[chunkhash].js';
}

module.exports = config;