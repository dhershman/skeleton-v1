'use strict';

var path		= require('path');
var webpack		= require('webpack');

module.exports = (config) => {

	var chunks = config._buildEntries(__dirname, []);

	config.resolve.alias['landing_main'] = path.resolve(__dirname, '_client');

	config.plugins.push(
		new webpack.optimize.CommonsChunkPlugin({
			name: 'commons.landing_main',
			minChunks: 3,
			chunks: chunks
		})
	);

	return config;
};
