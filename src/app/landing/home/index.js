'use strict';

module.exports = (request, reply) => {
	reply.view('landing/home/index', {
		css: {
			theme: 'default'
		}
	});
};