'use strict';

module.exports = (server) => {

	let preUser = (request, reply) => {
			//Temporary solution until I find a better theme system maybe like leonard in v6 me thinks.
			return reply({
				theme: (request.auth.artifacts) ? request.auth.artifacts.session.theme : 'default'
			});
	};

	let preAssets = (request, reply) => {
		let formatpath = request.path.replace('/', '').replace(/[/]/g, '-'); //remove first one and then replace the rest.
		console.log(`${formatpath}`);
		//CSS is temporarily blank until I get a good theme system in place to populate it properly,
		//I still want webpack to be able to handle it however so we will leave the
		//system in place until we can get a good theme system down
		return reply({
			js: {
				page: request.server.app.jsManifest[`${formatpath}`].js,
				manifest: request.server.app.jsManifest.manifest.js,
				vendor: request.server.app.jsManifest.vendor.js
			},
			css: {
				theme: request.server.app.cssManifest[request.auth.artifacts.session.theme],
				page: request.server.app.cssManifest[`${request.auth.artifacts.session.theme}.${formatpath}`]
			}
		});
	};

	server.route({
		path: '/',
		method: ['GET', 'POST'],
		handler: (request, reply) => {
			return reply.redirect('/home');
		}
	});

	server.route({
		path: '/{path*}',
		method: ['GET', 'POST'],
		handler: (request, reply) => {
			return require('../app' + request.path)(request, reply);
		},
		config: {
			pre:
			[
				[
					// executed in parallel, outside array is sync.
					{ method: preAssets, assign: 'assets' },
					{ method: preUser, assign: 'user' }
				]
			]
		}
	});

	return server;

};
