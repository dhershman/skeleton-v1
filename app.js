'use strict';

const hapi = require('hapi');
const debug = require('debug')('sky:main-app');

const server = new hapi.Server();
server.connection({
	port: 5000
});

//Sending server.register an array of plugins we want to use
server.register([{
		register: require('hapi-rethinkdb'),
		options: {
			url: 'rethinkdb://localhost:28015/test'
		}
	},
	require('vision'),
	require('inert'),
	require('./lib')

], (err) => {
	if (err) throw err;

	server.app.jsManifest = require('./src/config/js_manifest.json');
	server.app.cssManifest = require('./src/config/css_manifest.json');

	require('./src/server/routes.js')(server);

	server.views({
		engines: {
			hbs: require('handlebars')
		},
		path: './src/app',
		layoutPath: './src/templates/layouts',
		partialsPath: './src/templates/partials',
		layout: 'main'
	});

	server.start((err) => {
		if (err) throw err;
		debug('Server running at: ' + server.info.uri);
		server.log('info', 'Server running at: ' + server.info.uri);
	});

});